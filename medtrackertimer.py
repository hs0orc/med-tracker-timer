import time
import tkinter as tk
from tkinter import *
##run = raw_input("Start? > ")
import datetime

from threading import Timer
import json
##import vlc
from datetime import timedelta, date
##pygame.init()
from apscheduler.schedulers.background import BackgroundScheduler
# Only run if the user types in "start"
class med_tracker(Frame):
    def __init__(self, parent):
        Frame.__init__(self, parent)
        self.parent = parent
        self.initUI()
        self.tracking_list = []
        self.on_startup = True
        self.count = 0
        self.boolindicator = False

        self.sched = BackgroundScheduler()
        self.create_window()
        
    def create_top_right_notification(selfs):
        import subprocess
        subprocess.run(["notify-send", "meds cam"])
    def create_window(self):
##        top = self.top = tk.Toplevel(self)

        self.top = tk.Toplevel(self)
        self.myLabel = tk.Label(self.top, text='Did you take your meds this morning?')
        self.myLabel.pack()
##        self.myEntryBox = tk.Entry(top)
##        self.myEntryBox.pack()
        self.mySubmitButton = tk.Button(self.top, text='Yes', command=self.stop)
        self.mySubmitButton.pack()
        self.myNoButton = tk.Button(self.top, text='No', command=self.start)
        self.myNoButton.pack()


        
    def initUI(self):

        self.parent.title('Med Tracker 2019')
##        self.config(bg = '#F0F0F0')
        self.config(bg = '#D4D8DD')
        self.pack(fill = BOTH, expand = 1)

        button1 = Button(self, text = "Restart", command = self.create_window, anchor = W)
        button1.configure(width = 10, activebackground = "#33B5E5", relief = FLAT)
        button1.pack()

        button2 = Button(self, text = "Stop", command = self.stop_timer, anchor = W)
        button2.configure(width = 10, activebackground = "#33B5E5", relief = FLAT)
        button2.pack()


        
        self.mins = 0
        self.minsdict = {}
        self.stopped = True

    

    def daterange(self, start_date, end_date):
        for n in range(int ((end_date - start_date).days)):
            yield start_date + timedelta(n)

    def stop_timer(self):
        self.sched.shutdown()
    def start(self, *args, **kw): ## no button, starts again.
##        self.today_dict = {}
##        self.minsdict = {}
##        today_list = []
        #print('Take your meds!')
        #import datetime
        # x=datetime.datetime.now()
        # y=x.replace(day=x.day, hour=14, minute=32, second=1, microsecond=0)
        # delta_t=y-x

        # secs=delta_t.seconds+1

        # from datetime import datetime
        # import tkinter as tk
        from apscheduler.schedulers.background import BackgroundScheduler
        self.sched = BackgroundScheduler()

# Schedule job_function to be called every 1 second
# FIXME: Do not forget to change end_date to actual date
        self.sched.add_job(self.create_top_right_notification, 'interval', seconds=5, end_date="2028-04-04 14:53:20")

        self.sched.start()
        #if self.boolindicator:
        #    self.stop_timer()
       # def hello_world(*args, **kw):
        #    print('in hello_world')

            #t2 = Timer(secs, hello_world)
            #t2.start()
##            self.tracking_list = []
##            print('hello world', self.tracking_list)
        
       # hello_world()
        #t2 = Timer(secs, self.create_window)
        #t2.start()

        self.on_startup = False
        #timer_length = (int(y.hour) * 60 * 60)
        timer_length = 5
##        timer_length = 2
        self.top.destroy()

        self.now = datetime.datetime.now()

        self.current = str(self.now.year) + '-' + str(self.now.month) + '-' + str(self.now.day)

        self.tracking_list.append(timer_length)
        self.minsdict[self.current] = self.tracking_list
##        self.minsdict[current] = timer_length
##        for x in minsdict.keys():
##            if x != current:
##                self.count = 0
                
##        self.minsdict[current] = self.tracking_list
        
        self.write_data_file()
        print(self.minsdict)
        print(self.tracking_list)
##        print(now)

    def write_data_file(self):        
        pass

            
            
    def stop(self): ## yes button
##        self.stopped = True
##        self.write_data_file()
        # self.start()
        self.top.destroy()
        print('Good Job!')
        self.boolindicator = True

##        self.new_window()
##        print(self.minsdict)        


def main():
    root = Tk()
    root.geometry('200x100')
    app = med_tracker(root)
    app.mainloop()


if __name__ == '__main__':
    main()
